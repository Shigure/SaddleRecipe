/**
 * Created by Shigu on 13/03/2016.
 */
package me.shigu.saddlerecipe;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.Material;

public class SaddleRecipe extends JavaPlugin {

    @Override
    public void onEnable()
    {
        ItemStack saddle = new ItemStack(Material.SADDLE, 1);
        ShapedRecipe saddleRecipe = new ShapedRecipe(saddle);

        saddleRecipe.shape("lll", "s s", "   ");
        saddleRecipe.setIngredient('l', Material.LEATHER);
        saddleRecipe.setIngredient('s', Material.STRING);

        getServer().addRecipe(saddleRecipe);
    }

    @Override
    public void onDisable()
    {}
}
